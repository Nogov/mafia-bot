﻿using LuaBot;
using System.Collections.Generic;
using System.Linq;
using Telegram;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace MafiaBot
{
	public class MafiaBotHandler : LuaBotUpdatesHandler
	{
		private readonly Queue<long> _finishedGames = new Queue<long>();
		public Dictionary<long, MafiaGame> Games { get; } = new Dictionary<long, MafiaGame>();

		public MafiaBotHandler(TelegramBot bot, string rootPath)
			: base(bot, rootPath)
		{
			RegisterType<MafiaGame>();
			RegisterType<MafiaBotHandler>();
			Context.Add("game", default(MafiaGame));
			Context.Add("handler", this);
		}

		public void CreateGame(long id)
		{
			if (!Games.ContainsKey(id))
				Games.Add(id, new MafiaGame(id, Bot));
		}

		public override void Process(Update upd)
		{
			var chatId = GetChatId(upd);
			if (upd.Type != UpdateType.CallbackQueryUpdate)
			{
				MafiaGame game;
				Games.TryGetValue(chatId, out game);
				Context["game"] = game;
			}
			base.Process(upd);
		}

		public void Update()
		{
			foreach (var id in Games.Keys)
			{
				var game = Games[id];
				if (!game.Finished)
					game.Update();
				else
					_finishedGames.Enqueue(id);
			}
			if (_finishedGames.Any())
				Games.Remove(_finishedGames.Dequeue());
		}
	}
}