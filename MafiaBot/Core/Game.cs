﻿using System;

namespace MafiaBot.Core
{
	public abstract class Game
	{
		private long _previousTime;

		protected long _deltaTime;
		protected long _duration;
		
		public bool Started { get; private set; }
		public bool Finished { get; private set; }
		public string Uptime => new TimeSpan(_duration).ToString(@"hh\:mm\:ss");
		public abstract bool EndClause { get; }

		protected Game()
		{
			_previousTime = DateTime.UtcNow.Ticks;
		}

		public virtual void Update()
		{
			if (Finished)
				return;
			if (Started && EndClause)
				End();
			var current = DateTime.UtcNow.Ticks;
			_deltaTime = current - _previousTime;
			_duration += _deltaTime;
			_previousTime = current;
		}

		public virtual void Start()
		{
			Started = true;
		}

		public virtual void End()
		{
			Finished = true;
		}
	}
}