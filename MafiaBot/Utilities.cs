﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace MafiaBot
{
	public static class Utilities
	{
		private static readonly Random random = new Random();
		private static readonly object syncLock = new object();

		public static int GetRandom(int min, int max)
		{
			lock (syncLock)
			{
				return random.Next(min, max);
			}
		}

		private static string[] _names = {
			"Bob", "Dean", "Lucifer", "Duke", "Paul",
			"Seraph", "Abraham", "Cortney", "Louis",
			"Gloria", "Fidel", "Carlos", "Sophia",
			"Vong", "Bastile", "Serge", "Michael",
			"Emma", "Santiago", "Barbara", "Leon"
		};

		public static string GetRandomName()
		{
			var index = GetRandom(0, _names.Length);
			return _names[index];
		}

		public static IReplyMarkup GetInlineKeyboardMarkup<T>(IEnumerable<T> collection,
			Func<T, string> textSelector,
			Func<T, object> callbackDataSelector)
		{
			var keyboard = new InlineKeyboardButton[collection.Count()][];
			var i = -1;
			foreach (var item in collection)
				keyboard[++i] = new InlineKeyboardButton[1] {
					new InlineKeyboardButton(textSelector(item), callbackDataSelector(item).ToString())
				};
			return new InlineKeyboardMarkup(keyboard);
		}
	}
}