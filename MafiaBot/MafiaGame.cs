﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telegram;
using Telegram.Bot.Types.ReplyMarkups;

namespace MafiaBot
{
	using Core;
	using Roles;

	public partial class MafiaGame : Game
	{
		private long _timeSpent;
		private readonly long _chatId;

		protected TelegramBot _bot;
		
		public Phase CurrentPhase { get; private set; }
		public Dictionary<int, Player> Players { get; } = new Dictionary<int, Player>();
		public string Duration => new TimeSpan(_timeSpent).ToString(@"hh\:mm\:ss");
		public override bool EndClause
		{
			get
			{
				if (CurrentPhase is Initial)
					return false;
				var alive = Players.Values.Where(x => !x.IsDead);
				if (!alive.Any())
					return true;
				var mafia = alive.Count(x => x.Role is Mafia);
				var innocents = alive.Count(x => !(x.Role is Mafia));
				return mafia >= innocents || mafia == 0;
			}
		}

		public MafiaGame(long id, TelegramBot bot)
			: base()
		{
			_chatId = id;
			_bot = bot;
			CurrentPhase = new Initial(this);
			CurrentPhase.Notify();
		}

		private string GetPlayerScore(Player player)
			=> $"{player.Name} {player.Role.Name} [{(player.IsDead ? "dead" : "alive")}]";		

		private string GetScoreboard()
		{
			var sb = new StringBuilder(512);
			sb.AppendLine("Game ended.");
			foreach (var player in Players.Values)
				sb.AppendLine(GetPlayerScore(player));
			sb.AppendLine($"Time spent: {Duration}");
			return sb.ToString();
		}

		public Player GetPlayerById(int id) 
			=> Players[id];

		public void Notify(string message) 
			=> _bot.SendMessage(_chatId, message);

		public void Notify(Player player, string message) 
			=> _bot.SendMessage(player.Id, message);

		public void Notify(Player player, string message, IReplyMarkup markup)
		{
			_bot.SendMessage(new Dictionary<string, object> {
				{ "chat_id", player.Id },
				{ "text", message },
				{ "reply_markup", markup }
			});
		}

		public void NotifyPrivately(string message)
		{
			foreach (var player_id in Players.Keys)
				_bot.SendMessage(player_id, message);
		}

		public override void End()
		{
			base.End();
			Notify(GetScoreboard());
		}

		public override void Update()
		{
			base.Update();
			if (Started) _timeSpent += _deltaTime;
			if (CurrentPhase.TimeLeft > 0)
				CurrentPhase.TimeLeft -= _deltaTime;
			else if (!Finished)
			{
				CurrentPhase.End();
				if (Started && !EndClause)
				{
					CurrentPhase = CurrentPhase.Next();
					CurrentPhase.Notify();
				}
			}
		}

		public bool Join(Player player)
		{
			if (Players.ContainsKey(player.Id))
				return false;
			Players.Add(player.Id, player);
			return true;
		}

		public bool Join(int player_id, string name)
		{
			if (Players.ContainsKey(player_id))
				return false;
			Players.Add(player_id, new Player(player_id, name));
			return true;
		}

		public bool Leave(int player_id)
		{
			var result = Players.Remove(player_id);
			if (!Players.Any())
				End();
			return result;
		}

		public void PlayerAction(int player_id, string data)
		{
			var player = Players[player_id];
			var target = int.Parse(data);
			if (CurrentPhase is Voting)
				player.Role.Vote(target);
			else if (CurrentPhase is Night)
				(player.Role as INightActiveRole)?.UseAbility(target);
		}

		public void ForcePhase()
		{
			CurrentPhase.TimeLeft = 0;
		}

		public void ForceStart()
		{
			if (CurrentPhase is Initial)
				ForcePhase();
		}
	}
}