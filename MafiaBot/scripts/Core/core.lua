function getIsMessage(upd)
    return upd.Type == 1;
end

function getIsCallbackQuery(upd)
    return upd.Type == 4;
end

function append(line, newLine, delimeter)
    delimeter = delimeter or '\n';
    if newLine then
        return line .. delimeter .. newLine;
    end
end

function getPublicInfo(user)
    local result = 'Your public info:';
    result = append(result, 'ID: ' .. user.Id);
    if user.FirstName then
        result = append(result, user.FirstName);
    end
    if user.LastName then
        result = result .. ' ' .. user.LastName;
    end
    if user.Username then
        result = append(result, '@' .. user.Username);
    end
    return result;
end

function extractCmd(text)
    if not text then return nil end;
    local cmdPattern = "/(%w+)";
    local cmd = text:match(cmdPattern);
    return cmd;
end