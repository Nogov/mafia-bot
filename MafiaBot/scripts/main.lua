require("Core/core");
require("Logic/logic");
require("basic_responses");

local function processCommand(command)
    local user = upd.Message.From;
    if command == "start" then
        bot.SendMessage(startResponse);
    elseif command == "help" then    
        bot.SendMessage(helpResponse);
    elseif command == "whoami" then
        bot.SendMessage(chat_id, getPublicInfo(user));
    elseif command == "startgame" then
        handler.CreateGame(chat_id);
        --bot.SendMessage(chat_id, "Game has been created.\nUse /join to participate!");
    end
    if game then
        if command == "join" then
            if (game.Join(user.Id, getUsername(user))) then
                bot.SendMessage(getUserJoinedResponse(user));
            end
        elseif command == "flee" or command == "leave" then
            if (game.Leave(user.Id)) then
                bot.SendMessage(getUserLeftResponse(user));
            end 
        elseif command == "forcestart" then
            if not game.Started then
                game.ForceStart();
            end
        elseif command == "forcephase" then
            game.ForcePhase();
        elseif command == "uptime" then
            bot.SendMessage(chat_id, "Game's total uptime: " .. game.Uptime);
        elseif command == "duration" then
            bot.SendMessage(chat_id, "Game's duration: " .. game.Duration);
        elseif command == "drop" then
            game.End();
            bot.SendMessage(chat_id, "Match has been dropped.");
        end
    end
end

function getPlayersCountInfo()
    return "[" .. game.Players.Count .. "/" .. game.MIN_PLAYERS_COUNT .. "]";
end

function getUserJoinedResponse(user)
    return {
        chat_id = chat_id,
        text = getUsername(user) .. " has joined the game! " .. getPlayersCountInfo();
    };
end

function getUserLeftResponse(user)
    return {
        chat_id = chat_id,
        text = getUsername(user) .. " has left the game! " .. getPlayersCountInfo();
    };
end

function getUsername(user)
    return user.FirstName or user.LastName or user.UserName or "Nameless user ["..user.Id.."]";
end

if getIsMessage(upd) then
    local message = upd.Message;
    local cmd = extractCmd(message.Text)
    if cmd then
        processCommand(cmd);
    elseif message.NewChatMember then
        local newUser = message.NewChatMember;
        bot.SendMessage(chat_id, "Hello, " .. getUsername(newUser) .. "!");
    end
elseif getIsCallbackQuery(upd) then
    local callbackQuery = upd.CallbackQuery;    
    local response = {
        chat_id = chat_id,
        message_id = callbackQuery.Message.MessageId,
        text = "Time is up."
    };
    if game then
        game.PlayerAction(callbackQuery.From.Id, callbackQuery.Data);
        local player = game.GetPlayerById(tonumber(callbackQuery.Data));
        response.text = "You've choosen " .. player.Name .. ".";
    end
	bot.EditMessageText(response);
    bot.AnswerCallbackQuery(callbackQuery.Id, "Done");
end
