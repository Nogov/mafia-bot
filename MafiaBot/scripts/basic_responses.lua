tip = "Use /help to get more information.";
startResponse = {
    chat_id = chat_id,
    text = "Hello! Let's have some fun." .. '\n\n' .. tip;
}
helpResponse = {
    chat_id = chat_id,
    text = "Use /startgame to start a game."
}