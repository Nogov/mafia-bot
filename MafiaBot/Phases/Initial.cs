﻿namespace MafiaBot
{
	using Roles;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Reflection;
	using System.Text;
	using static MafiaGame;
	using static Utilities;

	public class Initial : Phase
	{
		public Initial(MafiaGame game)
			: base(game) { }

		public override string StartMessage
		{
			get
			{
				var sb = new StringBuilder(256);
				sb.AppendLine("A new game has begun!");
				sb.AppendLine("Invite your friends. Use /join to participate!");
				sb.AppendLine($"You have {TimeLeft / Constants.TICKS_PER_SEC} seconds before game starts.");
				sb.AppendLine("Use /forcestart to start immediately!");
				return sb.ToString();
			}
		}

		public override string EndMessage { get; } = "Prepare yourselves!";

		private void SetMafia()
		{
			var mafiaCount = (int)(Math.Ceiling(_game.Players.Count / Constants.MAFIA_DIFFICULTY_CFT));
			for (int i = 0; i < mafiaCount; i++)
			{
				var index = GetRandom(0, _game.Players.Count);
				var player = _game.Players.Values.ElementAt(index);
				player.Role = new Mafia(_game, player);
			}
		}

		private List<Type> GetRolesPool()
		{
			var assembly = Assembly.GetAssembly(typeof(Role));
			var types = assembly.GetTypes();
			var rolePool = new List<Type>();
			foreach (var type in types)
				if (type.IsDefined(typeof(RoleAttribute)))
					rolePool.Add(type);
			return rolePool;
		}

		private void SetupRoles()
		{
			SetMafia();
			var rolePool = GetRolesPool();
			foreach (var player in _game.Players.Values)
			{
				if (player.Role != null)
					continue;
				var index = GetRandom(0, rolePool.Count);
				var roleType = rolePool[index];
				player.Role = Activator.CreateInstance(roleType, _game, player) as Role;
				if (!roleType.GetCustomAttribute<RoleAttribute>().AllowMultiple)
					rolePool.Remove(roleType);
			}
		}

		public override Phase Next()
		{
			SetupRoles();
			return new Day(_game);
		}

		public override void End()
		{
			base.End();
			while (_game.Players.Count < Constants.MIN_PLAYERS_COUNT)
			{
				var name = GetRandomName();
				if (!_game.Players.Values.Any(p => string.Equals(p.Name, name)))
					_game.Join(new PlayerAI(name));
			}
			_game.Start();
		}
	}
}