﻿using System.Collections.Generic;
using System.Linq;

namespace MafiaBot
{
	using static MafiaGame;
	using static Utilities;

	public class Voting : Phase
	{
		public Dictionary<int, int> Votes { get; } = new Dictionary<int, int>();

		public Voting(MafiaGame game)
			: base(game) { }

		public override void Notify()
		{
			base.Notify();
			var text = "Who do you want to lynch?";
			var alive = _game.Players.Values.Where(player => !player.IsDead).ToArray();
			var markup = GetInlineKeyboardMarkup(alive, x => x.Name, x => x.Id);
			foreach (var player in alive)
			{
				if (player is PlayerAI)
				{
					var index = GetRandom(0, alive.Length);
					var accused = alive[index].Id;
					player.Role.Vote(accused);
				}
				else
					_game.Notify(player, text, markup);
			}
		}

		public override Phase Next() => new Night(_game);

		public override void End()
		{
			base.End();
			if (!Votes.Any())
				return;
			string message;
			var maxVotes = Votes.Values.Max();
			var playersWithMaxVotes = Votes.Where(x => x.Value == maxVotes).ToArray();
			if (playersWithMaxVotes.Length == 1)
			{
				var victimId = playersWithMaxVotes[0].Key;
				var victim = _game.Players[victimId];
				message = $"People decided to lynch {victim.Name}. {victim.Name} was {victim.Role.Name}.";
				victim.Die();
			}
			else
				message = $"People were unable to decide who to lynch.";
			_game.Notify(message);
		}		
	}
}