﻿namespace MafiaBot
{
	using System.Text;
	using static MafiaGame;

	public class Day : Phase
	{
		public Day(MafiaGame game)
			: base(game) { }

		private string GetPlayerInfo(Player player)
		{
			return player.IsDead
				? $"{player.Name} {player.Role.Name} [dead]"
				: $"{player.Name} [alive]";
		}
		
		private string GetGameState()
		{
			var sb = new StringBuilder(512);
			foreach (var player in _game.Players.Values)
				sb.AppendLine(GetPlayerInfo(player));
			return sb.ToString();
		}

		public override Phase Next() => new Voting(_game);
		
		public override void Notify()
		{
			base.Notify();
			var message = GetGameState();
			_game.Notify(message);
		}
	}
}