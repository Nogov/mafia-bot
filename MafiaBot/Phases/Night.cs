﻿namespace MafiaBot
{
	using Roles;
	using System.Linq;
	using static MafiaGame;

	public class Night : Phase
	{
		public Night(MafiaGame game)
			: base(game) { }

		public override Phase Next() => new Day(_game);
		
		public override void Notify()
		{
			base.Notify();
			var alivePlayersRoles = _game.Players.Values
				.Where(player => !player.IsDead)
				.Select(player => player.Role);
			foreach (var playerRole in alivePlayersRoles.OfType<INightActiveRole>())
				playerRole.Suggest();
		}
	}
}