﻿namespace MafiaBot
{
	internal static class Constants
	{
		internal const int TICKS_PER_SEC = 10000000;
		internal const int DEFAULT_PHASE_DURATION = 90;
		internal const int MIN_PLAYERS_COUNT = 5;
		internal const float MAFIA_DIFFICULTY_CFT = 3.5f;
	}
}
