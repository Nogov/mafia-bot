﻿namespace System.Linq
{
	using Collections.Generic;

	public static class Extensions
	{
		public static void ForEach<T>(this IEnumerable<T> src, Action<T> action) {
			foreach (var el in src)
				action(el);
		}
	}
}
