﻿using System;

namespace MafiaBot
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	internal class RoleAttribute : Attribute
	{
		public bool AllowMultiple { get; private set; }
		public RoleAttribute() { }

		public RoleAttribute(bool allowMultiple)
		{
			AllowMultiple = allowMultiple;
		}
	}
}