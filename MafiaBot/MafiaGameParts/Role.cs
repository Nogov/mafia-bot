﻿using System.Linq;

namespace MafiaBot
{
	public partial class MafiaGame
	{
		public abstract class Role
		{
			protected Player _owner;
			protected MafiaGame _game;
			protected virtual string Description => $"You're {Name}.";

			public virtual string Name => GetType().Name;

			protected Role(MafiaGame game, Player owner)
			{
				_game = game;
				_owner = owner;
				_game.Notify(_owner, Description);
			}

			public void Vote(int accusedId)
			{
				var votes = ((Voting)_game.CurrentPhase).Votes;
				if (votes.ContainsKey(accusedId))
					votes[accusedId]++;
				else
					votes.Add(accusedId, 1);
				if (votes.Values.Sum() == _game.Players.Values.Count(p => !p.IsDead))
					_game.ForcePhase();
				_game.Notify($"{_owner.Name} voted for {_game.Players[accusedId].Name}.");
			}

			public virtual void Lynch() { }
		}
	}
}