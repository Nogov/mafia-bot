﻿namespace MafiaBot
{
	using static MafiaGame;

	public class Player
	{
		public int Id { get; private set; }
		public string Name { get; private set; }
		public Role Role { get; set; }
		public bool IsDead { get; private set; }

		public Player(int id, string name)
		{
			Id = id;
			Name = name;
		}

		public void Die()
		{
			IsDead = true;
			Role.Lynch();
		}

		public override int GetHashCode() => Id;
		public override bool Equals(object obj)
		{
			var other = obj as Player;
			return (other != null) && Id == other.Id;
		}
	}
}