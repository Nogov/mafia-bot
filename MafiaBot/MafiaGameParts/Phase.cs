﻿using System.Globalization;
using System.Text;
using Telegram;

namespace MafiaBot
{
	public partial class MafiaGame
	{
		public abstract class Phase
		{
			private static readonly CultureInfo _culture = CultureInfo.CurrentCulture;

			protected MafiaGame _game;
			protected long GameId => _game._chatId;
			protected TelegramBot Bot => _game._bot;

			public long TimeLeft { get; set; }

			public virtual string StartMessage
			{
				get
				{
					var sb = new StringBuilder(256);
					sb.AppendLine($"A new {GetType().Name.ToLower(_culture)} has begun!");
					sb.AppendLine($"You have {TimeLeft / Constants.TICKS_PER_SEC} seconds to do your stuff.");
					return sb.ToString();
				}
			}

			public virtual string EndMessage => $"The {GetType().Name.ToLower(_culture)} is over.";

			protected Phase(MafiaGame game)
			{
				_game = game;
				TimeLeft = Constants.DEFAULT_PHASE_DURATION * Constants.TICKS_PER_SEC;
			}

			public virtual void Notify()
			{
				_game.Notify(StartMessage);
			}

			public abstract Phase Next();

			public virtual void End()
			{
				_game.Notify(EndMessage);
			}
		}
	}
}