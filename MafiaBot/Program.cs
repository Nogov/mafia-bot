﻿using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Telegram;

namespace MafiaBot
{
	public static class Program
	{
		private static TelegramBot GetBot()
		{
			var token = ConfigurationManager.AppSettings["token"];
			return new TelegramBot(token);
		}

		private static string GetRootPath()
		{
			var workingPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			var scriptsPath = ConfigurationManager.AppSettings["scriptsPath"];
			return Path.Combine(workingPath, scriptsPath);
		}

		private static Job CreateUpdatesReadingJob(BotUpdatesHandler handler)
		{
			return () => handler.Bot.GetUpdates(handler.Offset).ForEach(handler.Process);
		}

		private static void RunApp(MafiaBotHandler handler)
		{
			var app = new AppEngine(2);
			app.Jobs += CreateUpdatesReadingJob(handler);
			app.Jobs += handler.Update;
			app.Run();
		}

		public static void Main()
		{
			var telegramBot = GetBot();
			var rootPath = GetRootPath();
			var handler = new MafiaBotHandler(telegramBot, rootPath);

			RunApp(handler);

			Thread.Sleep(-1);
		}
	}
}
