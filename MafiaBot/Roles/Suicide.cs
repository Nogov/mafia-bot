﻿namespace MafiaBot.Roles
{
	using static MafiaGame;

	[Role(allowMultiple: false)]
	public class Suicide : Role
	{
		public override string Name => "suicide";

		public Suicide(MafiaGame game, Player owner)
			: base(game, owner) { }

		public override void Lynch()
		{
			base.Lynch();
			_game.End();
		}
	}
}
