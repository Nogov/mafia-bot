﻿using System.Linq;

namespace MafiaBot.Roles
{
	using static MafiaGame;
	using static Utilities;

	public class Mafia : Role, INightActiveRole
	{
		public override string Name => "mafia";

		public Mafia(MafiaGame game, Player owner)
			: base(game, owner) { }

		public string GetAbilityMessage() => "Who do you want to murder tonight?";

		public void Suggest()
		{
			var abilityMsg = GetAbilityMessage();
			var alive = _game.Players.Values.Where(p => !p.IsDead);
			var keyboard = GetInlineKeyboardMarkup(alive, p => p.Name, p => p.Id);
			_game.Notify(_owner, abilityMsg, keyboard);
		}

		public void UseAbility(int targetId)
		{
			var victim = _game.Players[targetId];
			victim.Die();
			_game.Notify(victim, "You were killed tonight. Rest in peace.");
			_game.Notify($"{victim.Name} was killed tonight. {victim.Name} was {victim.Role.Name}.");
		}
	}
}
