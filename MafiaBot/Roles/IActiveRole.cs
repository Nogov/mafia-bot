﻿namespace MafiaBot.Roles
{
	internal interface IActiveRole
	{
		void Suggest();
		void UseAbility(int target);
		string GetAbilityMessage();
	}
}