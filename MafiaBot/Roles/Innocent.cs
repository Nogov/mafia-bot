﻿namespace MafiaBot.Roles
{
	using static MafiaGame;

	[Role(allowMultiple: true)]
	public class Innocent : Role
	{
		public override string Name => "innocent";

		public Innocent(MafiaGame game, Player owner)
			: base(game, owner) { }
	}
}
