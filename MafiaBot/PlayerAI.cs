﻿namespace MafiaBot
{
	public class PlayerAI : Player
	{
		private static int _lastPseudoId;
		private static int _pseudoId => --_lastPseudoId;

		public PlayerAI(string name)
			: base(_pseudoId, name) { }
	}
}