﻿using MoonSharp.Interpreter;
using System;
using Telegram;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace LuaBot.Extensions
{
	public static class LuaBotExtensions
    {
		// TODO: Oh, God, it must be easier!
        public static InlineKeyboardMarkup GetInlineKeyboardMarkup(this TelegramBot bot,
            DynValue[] dynButtons, int columnsCount)
        {
            var buttonsCount = dynButtons.Length;
            if (columnsCount == 0 || buttonsCount == 0)
				return default(InlineKeyboardMarkup);
            var height = (buttonsCount > columnsCount) 
				? buttonsCount / columnsCount
				: 1;
            var inlineKeyboardButtons = new InlineKeyboardButton[height][];
            int i = 0, j = 0, buttonsLeft = buttonsCount;
            foreach (var dynButton in dynButtons)
            {
                if (inlineKeyboardButtons[i] == null)
                {
                    var width = Math.Min(buttonsLeft, columnsCount);
                    inlineKeyboardButtons[i] = new InlineKeyboardButton[width];
                }
                var button = dynButton.Table.ToObject<InlineKeyboardButton>();
                if (string.IsNullOrWhiteSpace(button.CallbackData))
                    button.CallbackData = button.Text;
                inlineKeyboardButtons[i][j] = button;
                j++;
                if (j >= columnsCount) { j = 0; i++; }
                if (i >= height) break;
            }
            return new InlineKeyboardMarkup(inlineKeyboardButtons);
        }

        public static T ToObject<T>(this Table t)
            where T : new()
        {
            T obj = new T();
            Type type = obj.GetType();
            foreach (var prop in type.GetProperties())
            {
                var propName = prop.Name;
                if (t[propName] != null)
                    prop.SetValue(obj, t[propName]);
            }
            return obj;
        }
    }
}