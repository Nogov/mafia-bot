﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Loaders;
using System;
using System.Collections.Generic;
using System.IO;
using Telegram;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using SysFile = System.IO.File;

namespace LuaBot
{
	using Extensions;

	public class LuaBotUpdatesHandler : BotUpdatesHandler
    {
        private readonly string _rootPath;
        private readonly string[] _modulePaths;
        private readonly string _entryPointFilename = "main.lua";
        private string MainCodeFilename { get { return $@"{_rootPath}\{_entryPointFilename}"; } }

        public List<UpdateType> UnsupportedTypes { get; set; } =
            new List<UpdateType> {
                UpdateType.EditedMessage,
                UpdateType.InlineQueryUpdate
            };

        public Dictionary<string, object> Context { get; set; } =
            new Dictionary<string, object> {
                { "upd", default(Update) },
                { "chat_id", default(long) },
                { "bot", default(TelegramBot) }
            };

        public LuaBotUpdatesHandler(TelegramBot bot, string rootPath)
            : base(bot)
        {
            _rootPath = rootPath;

            if (!SysFile.Exists(MainCodeFilename))
                throw new FileNotFoundException($"Script not found", MainCodeFilename);

            _modulePaths = ScriptLoaderBase.UnpackStringPaths($@"{_rootPath}/?;{_rootPath}/?.lua");

            RegisterType(typeof(TelegramBot));
            RegisterType(typeof(Update));
            RegisterType(typeof(InlineKeyboardMarkup));
            RegisterExtensionType(typeof(LuaBotExtensions));

            Context["bot"] = Bot;
        }

        private bool GetIsUnsupportedUpdateType(UpdateType updateType)
        {
            return UnsupportedTypes.Contains(updateType);
        }

        private Script PrebakeSript(Dictionary<string, object> context)
        {
            var script = new Script(CoreModules.Preset_Complete);
            ((ScriptLoaderBase)script.Options.ScriptLoader).ModulePaths = _modulePaths;
            foreach (var key in context.Keys)
                script.Globals[key] = context[key];
            return script;
        }

		protected void RegisterType<T>()
		{
			RegisterType(typeof(T));
		}

		protected void RegisterType(Type type)
        {
            if (!UserData.IsTypeRegistered(type))
                UserData.RegisterType(type);
            foreach (var prop in type.GetProperties())
            {
                var propType = prop.PropertyType;
                if (UserData.IsTypeRegistered(propType))
					continue;
                if (propType.GetProperties().Length > 0)
                    RegisterType(propType);
            }
        }

		protected void RegisterStaticType<T>(string alias = null)
		{
			if (!UserData.IsTypeRegistered<T>())
				UserData.RegisterType<T>();
			var dynValue = UserData.CreateStatic<T>();
			var key = string.IsNullOrEmpty(alias) ? typeof(T).Name : alias;
			Context.Add(key, dynValue);
		}

        protected void RegisterExtensionType(Type type)
        {
            UserData.RegisterExtensionType(type);
        }

        protected void ExecuteScript(Update upd)
        {
            try
            {
                var chat_id = GetChatId(upd);
                Context["upd"] = upd;
                Context["chat_id"] = chat_id;
                var script = PrebakeSript(Context);
                script.DoFile(MainCodeFilename);
            }
            catch (InterpreterException e)
            {
                Console.WriteLine(e.DecoratedMessage);
            }
        }

		public override void Process(Update upd)
		{
			if (GetIsUnsupportedUpdateType(upd.Type))
				return;
			ExecuteScript(upd);
            base.Process(upd);
        }
    }
}