﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Telegram
{
	using Bot.Types;

	public abstract class ApiRequest<T>
	{
		private string URL => $"https://api.telegram.org/bot{Token}/{Method}";

		protected string Token { get; }
		protected virtual string Method => GetType().Name;

		protected ApiRequest(string token) {
			Token = token;
		}

        public async virtual Task<T> Execute(Dictionary<string, object> args)
        {
            var uri = new Uri(URL);
            HttpResponseMessage response;
            using (var client = new HttpClient())
                response = await client.PostAsJsonAsync(uri, args);
            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ApiResponse<T>>(responseString).ResultObject;
        }
	}
}