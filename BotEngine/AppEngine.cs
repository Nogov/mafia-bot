﻿using System;

namespace Telegram
{
	public delegate void Job();

	public class AppEngine
	{
		private readonly long _updDelay;

		public Job Jobs { get; set; } = () => { };
        public bool EnableVerboseMode { get; set; }

        public AppEngine(int updateDelay)
        {
			_updDelay = updateDelay * Constants.TICKS_PER_SEC;
        }

		private long GetCurrentTime() => DateTime.Now.Ticks;

        private void Update()
        {
            if (EnableVerboseMode) Console.WriteLine("Jobs started.");
            Jobs();
            if (EnableVerboseMode) Console.WriteLine("Jobs ended.");
        }

        public void Run()
        {
            var previous = GetCurrentTime();
            while (true)
            {
                var current = GetCurrentTime();
                var elapsed = current - previous;
                if (elapsed > _updDelay)
                {
                    Update();
                    previous = current;
                }
            }
        }
    }
}