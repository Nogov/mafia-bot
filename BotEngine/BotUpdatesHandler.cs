﻿namespace Telegram
{
	using Bot.Types;
	using Bot.Types.Enums;
	using System;

	public abstract class BotUpdatesHandler
    {
        private int _lastUpdateId;

		public int Offset => _lastUpdateId + 1;
		public TelegramBot Bot { get; protected set; }
		
        protected BotUpdatesHandler(TelegramBot bot)
        {
            Bot = bot;
        }

        public virtual void Process(Update upd)
        {
            _lastUpdateId = upd.Id;
        }

        protected long GetChatId(Update upd)
        {
            switch (upd.Type)
            {
                case UpdateType.MessageUpdate:
                    return upd.Message.Chat.Id;
                case UpdateType.CallbackQueryUpdate:
                    return upd.CallbackQuery.From.Id;
                case UpdateType.InlineQueryUpdate:
                    return upd.InlineQuery.From.Id;
                case UpdateType.ChosenInlineResultUpdate:
                    return upd.ChosenInlineResult.From.Id;
                case UpdateType.EditedMessage:
                    return upd.EditedMessage.From.Id;
                default: throw new ArgumentException("Unknown update type", nameof(upd));
            }
        }
    }
}