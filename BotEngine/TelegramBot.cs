﻿using System.Collections.Generic;

namespace Telegram
{
	using Bot.Types;
	using RequestMethods;

	public class TelegramBot
	{
		private readonly string _token;

		public TelegramBot(string token)
		{
			_token = token;
		}

		#region GetUpdates

		public virtual IEnumerable<Update> GetUpdates(int lastUpdateId = 0)
			=> new GetUpdates(_token).Execute(lastUpdateId).Result;

		#endregion

		#region SendMessage

		public virtual Message SendMessage(long chat_id, string text)
			=> new SendMessage(_token).Execute(chat_id, text).Result;

		public virtual Message SendMessage(string chat_id, string text)
			=> new SendMessage(_token).Execute(chat_id, text).Result;

		public virtual Message SendMessage(Dictionary<string, object> args)
			=> new SendMessage(_token).Execute(args).Result;

		public virtual Message SendMessage(long chat_id, string text, Dictionary<string, object> args)
			=> new SendMessage(_token).Execute(chat_id, text, args).Result;

		#endregion


		#region AnswerCallbackQuery

		public virtual bool AnswerCallbackQuery(string callbackQueryId)
			=> new AnswerCallbackQuery(_token).Execute(callbackQueryId).Result;

		public virtual bool AnswerCallbackQuery(string callbackQueryId, string text)
			=> new AnswerCallbackQuery(_token).Execute(callbackQueryId, text).Result;

		public virtual bool AnswerCallbackQuery(Dictionary<string, object> args)
			=> new AnswerCallbackQuery(_token).Execute(args).Result;

		public virtual bool AnswerCallbackQuery(string callbackQueryId, Dictionary<string, object> args)
			=> new AnswerCallbackQuery(_token).Execute(callbackQueryId, args).Result;

		#endregion

		#region EditMessageText

		public virtual Message EditMessageText(Dictionary<string, object> args)
			=> new EditMessageText(_token).Execute(args).Result;

		public virtual Message EditMessageText(long chat_id, long message_id, string text, Dictionary<string, object> args)
			=> new EditMessageText(_token).Execute(text, args).Result;

		public virtual Message EditMessageText(string text, Dictionary<string, object> args)
			=> new EditMessageText(_token).Execute(text, args).Result;

		#endregion
	}
}