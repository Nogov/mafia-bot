﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Telegram.RequestMethods
{
    public class AnswerCallbackQuery : ApiRequest<bool>
    {
        public AnswerCallbackQuery(string token)
            : base(token)
        {
        }

        public async Task<bool> Execute(string callback_query_id)
        {
            var args = new Dictionary<string, object> {
                { "callback_query_id", callback_query_id }
            };
            return await Execute(args);
        }

        public async Task<bool> Execute(string callback_query_id, string text)
        {
            var args = new Dictionary<string, object> {
                { "callback_query_id", callback_query_id },
                { "text", text }
            };
            return await Execute(args);
        }

        public async Task<bool> Execute(string callback_query_id, Dictionary<string, object> args)
        {
            args.Add("callback_query_id", callback_query_id);
            return await Execute(args);
        }
    }
}
