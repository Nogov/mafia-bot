﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.RequestMethods
{
    public class SendMessage : ApiRequest<Message>
    {
        public SendMessage(string token)
            : base(token)
        {
        }

        public async Task<Message> Execute(string chat_id, string text)
        {
            var args = new Dictionary<string, object> {
                { "chat_id", chat_id },
                { "text", text }
            };
            return await Execute(args);
        }

        public async Task<Message> Execute(long chat_id, string text)
        {
            var args = new Dictionary<string, object> {
                { "chat_id", chat_id },
                { "text", text }
            };
            return await Execute(args);
		}

		public async Task<Message> Execute(long chat_id, string text, Dictionary<string, object> args)
        {
            args.Add("chat_id", chat_id);
            args.Add("text", text);
            return await Execute(args);
		}
	}
}
