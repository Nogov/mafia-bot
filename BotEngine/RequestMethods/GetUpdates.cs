﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Telegram.RequestMethods
{
    public class GetUpdates : ApiRequest<Update[]>
    {
        public GetUpdates(string token)
            : base(token)
        {
        }

        public async Task<Update[]> Execute(int offset)
        {
            var args = new Dictionary<string, object> {
                { "offset", offset }
            };
            return await Execute(args);
        }
    }
}