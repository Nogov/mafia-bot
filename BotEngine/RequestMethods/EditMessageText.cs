﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Telegram.RequestMethods
{
	public class EditMessageText : ApiRequest<Message>
	{
		public EditMessageText(string token)
			: base(token)
		{
		}

		public async Task<Message> Execute(string text)
		{
			var args = new Dictionary<string, object> {
				 { "text", text }
			 };
			return await Execute(args);
		}

		public async Task<Message> Execute(string text, Dictionary<string, object> args)
		{
			args.Add("text", text);
			return await Execute(args);
		}
	}
}