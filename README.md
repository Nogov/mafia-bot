# README #

This is a well-known MAFIA game for Telegram.
It was a project made by inspiration in a very short period of time. Because of this you may feel the lack of documentation (my apologies).

This project uses
 
* Primitive game-development pattern Game Cycle (implemented in AppEngine.cs)
* Telegram Bots API
* Lua interpreter (via MoonSharp) as a "client-side" logic but the idea isn't as cool as I expected